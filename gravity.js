//Gravity constructor
var Gravity = function () {
//Canvas/context
   this.canvas = document.getElementById('game-canvas'),
   this.context = this.canvas.getContext('2d'),

//Constants
   this.TOP_PLAYER_TRACK_Y = 8,
   this.BOTTOM_PLAYER_TRACK_Y = 360,

   this.TOP_TRACK_ID = 0,
   this.BOTTOM_TRACK_ID = 1,
   this.MID_TRACK_ID = 2,

   this.STARTING_PLAYER_LEFT = 50,
   this.playing = true,

   this.STARTING_PLAYER_VELOCITY = 0,
   this.PLAYER_HEIGHT = 32,
   this.PLAYER_CELLS_HEIGHT  = 32,
   this.PLAYER_CELLS_WIDTH = 32,

   this.PLAYER_DIRECTION_DESCEND = 0,
   this.PLAYER_DIRECTION_ASCEND = 1,
   this.PLAYER_DIRECTION_MID = 2,

   this.OBSTRUCTION_CELLS_HEIGHT = 236,
   this.OBSTRUCTION_CELLS_WIDTH = 57,
   this.OBSTRUCTION_DISTANCE = 300,
   this.STARTING_DISTANCE_OBSTRUCTIONS = 500,
   this.TOP_OBSTRUCTION_TRACK_Y = -150,
   this.BOTTOM_OBSTRUCTION_TRACK_Y = 300,

   this.SPECIAL_OBSTRUCTION_DISTANCE = 400,
   this.SPECIAL_OBSTRUCTION_FREQUENCY_CHANCE = 5,
   this.SPECIAL_OBSTRUCTION_CELLS_HEIGHT = 23 - 8,
   this.SPECIAL_OBSTRUCTION_CELLS_WIDTH = 45,
   this.SPECIAL_OBSTRUCTION_TOP_Y = 0;

   this.BUTTON_CELLS_WIDTH = 86,
   this.BUTTON_CELLS_HEIGHT = 28,
   this.BUTTON_DISTANCE_FROM_OBSTRUCTION = 100,
   this.TOP_BUTTON_TRACK_Y = -2;
   this.BOTTOM_BUTTON_TRACK_Y = 375,

   this.LIGHTNING_CELLS_WIDTH = 68,
   this.LIGHTNING_CELLS_HEIGHT = 365,
   this.LIGHTNING_TRACK_Y = this.SPECIAL_OBSTRUCTION_CELLS_HEIGHT,
   this.lightningPlaying = false,

   this.POWER_UP_CELLS_HEIGHT = 55,
   this.POWER_UP_CELLS_WIDTH = 52,
   this.POWER_UP_FREQUENCY = 3000,
   this.POWER_UP_TRACK_Y = 200,
   this.STARTING_DISTANCE_POWER_UPS = 100,

   this.EXPLOSION_CELLS_HEIGHT = 62,
   this.EXPLOSION_DURATION = 500,

   this.BACKGROUND_VELOCITY = 60,

   this.PILLAR_VELOCITY_MULTIPLIER = 1.3,
   this.PLANET_VELOCITY_MULTIPLIER = 0.1,
   this.SPACE_VELOCITY_MULTIPLIER = 0.05,
   this.OBSTRUCTION_VELOCITY_MULTIPLIER = 3,

   this.STARTING_BACKGROUND_OFFSET = 0,
   this.STARTING_WALL_OFFSET = 0,
   this.STARTING_PILLAR_OFFSET = 0,
   this.STARTING_PLANET_OFFSET = 0,
   this.STARTING_SPACE_OFFSET = 0,
   this.STARTING_OBSTRUCTION_OFFSET = 0,

   this.PAUSE_CHECK_INTERVAL = 200,
   this.DEFAULT_TOAST_TIME = 750,
   this.RESTART_TIME = 250,

   this.level = 1;

   this.timeRate = 1.0,
   this.timeSystem = new TimeSystem(),

   this.playerHasSheild = false,
   this.sameSideObstacle = 0,

   this.side = false,
   this.TOP_OBSTRUCTION = true,
   this.BOTTOM_OBSTRUCTION = false,

//Pause
   this.paused = false,
   this.pauseStartTime = 0,
   this.totalTimePaused = 0,

   this.windowHasFocus = true,

   this.ascendStartTime = 0,
   this.descendStartTime = 0,
   this.deltaY = 0,

//Tracks
   this.playerTrack = this.BOTTOM_TRACK_ID,
   this.playerDirection = this.PLAYER_DIRECTION_MID,

//Images
   this.background1  = new Image(),
   this.background2  = new Image(),
   this.background3  = new Image(),
   this.backgroundPillars  = new Image(),
   this.planet  = new Image(),
   this.space  = new Image(),
   this.pauseButton = new Image(),
   this.goCountOutImage = new Image(),
   this.spritesheet = new Image(),

//Animation/time
   this.lastAnimationFrameTime = 0,
   this.lastFpsUpdateTime = 0,
   this.fps = 60,

//Elements
   this.fpsElement = document.getElementById('fps'),
   this.toast = document.getElementById('toast'),
   this.scoreElement = document.getElementById('score'),
   this.pauseMenu = document.getElementById('pauseMenu'),
   this.highscoreElement = document.getElementById('highscore'),
   this.mobile,
   this.score = 0,
   this.highscore = 0,
   this.POINTS_MULTIPLIER = 1,

//Music N' Sound
   this.soundAndMusicElement = document.getElementById('sound-and-music'),
   this.musicElement = document.getElementById('switcher-music'),
   this.musicElement.volume = 0.1,
   this.audioSprites= document.getElementById('switcher-audio-sprites'),
   this.soundOn = true,

   this.audioChannels = [
      {playing: false, audio: this.audioSprites,},
      {playing: false, audio: null,},
      {playing: false, audio: null,},
      {playing: false, audio: null}],

   this.audioSpriteCountdown = this.audioChannels.length - 1,
   this.graphicsReady = false,

//Offsets
   this.backgroundOffset = this.STARTING_BACKGROUND_OFFSET,
   this.spriteOffset = this.STARTING_BACKGROUND_OFFSET,
   this.pillarOffset = this.STARTING_PILLAR_OFFSET,
   this.planetOffset = this.STARTING_PLANET_OFFSET,
   this.spaceOffset = this.STARTING_SPACE_OFFSET,
   this.obstructionOffset = this.STARTING_OBSTRUCTION_OFFSET,
   this.buttonOffset = this.STARTING_OBSTRUCTION_OFFSET,
   this.lightningOffset = this.STARTING_OBSTRUCTION_OFFSET,
   this.powerUpOffset = this.STARTING_OBSTRUCTION_OFFSET,
   
   this.nextObstructionLeft = this.STARTING_DISTANCE_OBSTRUCTIONS,
   this.nextPowerUpLeft = this.STARTING_DISTANCE_POWER_UPS,
   this.twoPassed = 0,
   this.onePassed = 0,

//Velocity
   this.bgVelocity = this.BACKGROUND_VELOCITY,
   this.pillarVelocity,
   this.planetVelocity,
   this.spaceVelocity,
   this.wallVelocity,
   this.obstructionVelocity,
   this.specailObstructionVelocity,
   this.DIFFICULTY_MULTIPLIER = 2,
   this.VELOCITY_DEDUCTION = 0,


//Sounds
   this.slowDownTimeSound = 
   {
      position: 0.1, //seconds
      duration: 1300, //milliseconds
      volume: 0.5
   },
   this.speedUpTimeSound = 
   {
      position: 1.45, //seconds
      duration: 1300, //milliseconds
      volume: 0.5
   },
   this.explodingSound =
   {
      position: 7.95, //seconds
      duration: 1100, //milliseconds
      volume: 1.0
   },
   this.switchSound =
   {
      position: 10, //seconds
      duration: 500, //milliseconds
      volume: 0.5
   },
   this.twoTimesSound =
   {
      position: 10.925, //seconds
      duration: 600, //milliseconds
      volume: 0.75
   },
   this.shieldPowerOnSound =
   {
      position: 12.60, //seconds
      duration: 1300, //milliseconds
      volume: 0.75
   },
   this.shieldPowerOffSound =
   {
      position: 14.15, //seconds
      duration: 1000, //milliseconds
      volume: 0.5
   },
   this.buttonSound =
   {
      position: 15.50, //seconds
      duration: 400, //milliseconds
      volume: 0.5
   },
   this.miscSound =
   {
      position: 16, //seconds
      duration: 600, //milliseconds
      volume: 0.5
   },
   this.lightSound =
   {
      position: 16.6, //seconds
      duration: 600, //milliseconds
      volume: 0.5
   },


//Sprite Cells
   this.playerCells = [
      { left: 3,   top: 135, width: this.PLAYER_CELLS_WIDTH,
                             height: this.PLAYER_CELLS_HEIGHT },

      { left: 39,  top: 135, width: this.PLAYER_CELLS_WIDTH, 
                             height: this.PLAYER_CELLS_HEIGHT },

      { left: 76,  top: 135, width: this.PLAYER_CELLS_WIDTH, 
                             height: this.PLAYER_CELLS_HEIGHT },

      { left: 112, top: 135, width: this.PLAYER_CELLS_WIDTH, 
                             height: this.PLAYER_CELLS_HEIGHT },

      { left: 148, top: 135, width: this.PLAYER_CELLS_WIDTH, 
                             height: this.PLAYER_CELLS_HEIGHT }
   ],

   this.explosionCells = [
      { left: 1,   top: 48, width: 50, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 60,  top: 48, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 143, top: 48, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 230, top: 48, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 305, top: 48, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 389, top: 48, width: 68, height: this.EXPLOSION_CELLS_HEIGHT },
      { left: 470, top: 48, width: 68, height: this.EXPLOSION_CELLS_HEIGHT }
   ],

   this.obstructionYellowCells = [
      { left: 215, top: 185, width: 57, height: 236 }
   ],

   this.obstructionOrangeCells = [
      { left: 288, top: 185, width: 57, height: 236 }
   ],
   this.obstructionRedCells = [
      { left: 361, top: 185, width: 57, height: 236 }
   ],

   this.buttonUnpressedBottomCells = [
      { left: 214, top: 446, width: 86, height: 28 }
   ],

   this.buttonPressedBottomCells = [
      { left: 333, top: 446, width: 85, height: 28 }
   ],

   this.buttonUnpressedTopCells = [
      { left: 333, top: 487, width: 86, height: 30 }
   ],

   this.buttonPressedTopCells = [
      { left: 333, top: 526, width: 86, height: 25 }
   ],

   this.lightningCells = [
      { left: 9, top: 185, width: 68, height: 365 },
      { left: 80, top: 185, width: 62, height: 365 },
      { left: 146, top: 185, width: 57, height: 365 }
   ],

   this.electroSourceTopCells = [
      { left: 215, top: 490, width: 45, height: 25 }
   ],

   this.electroSourceBottomCells = [
      { left: 215, top: 527, width: 45, height: 23 }
   ],

   this.slowDownPowerUpCells = [
      { left: 366,   top: 126, width: 46, height: 53 }
   ],

   this.doublePointsPowerUpCells = [
      { left: 292,   top: 126, width: 52, height: 54 }
   ],

   this.shieldPowerUpCells = [
      { left: 220,   top: 125, width: 48, height: 55 }
   ],

   this.playerShieldCells = [
      { left: 277,   top: 501, width: 37, height: 38 }
   ],

   this.obstructions = [],
   this.specailBottoms = [],
   this.buttons = [],
   this.powerUps = [],
   this.lightnings = [],
   this.sprites = [],


   this.obstructionYellowArtist = new SpriteSheetArtist(this.spritesheet, this.obstructionYellowCells),  
   this.obstructionOrangeArtist = new SpriteSheetArtist(this.spritesheet, this.obstructionOrangeCells), 
   this.obstructionRedArtist = new SpriteSheetArtist(this.spritesheet, this.obstructionRedCells),      
   this.specialObstructionTopArtist = new SpriteSheetArtist(this.spritesheet, this.electroSourceTopCells),
   this.specialObstructionBottomArtist = new SpriteSheetArtist(this.spritesheet, this.electroSourceBottomCells),
   this.buttonTopArtist = new SpriteSheetArtist(this.spritesheet, this.buttonUnpressedTopCells),
   this.buttonTopPressedArtist = new SpriteSheetArtist(this.spritesheet, this.buttonPressedTopCells),
   this.buttonBottomArtist = new SpriteSheetArtist(this.spritesheet, this.buttonUnpressedBottomCells),
   this.buttonBottomPressedArtist = new SpriteSheetArtist(this.spritesheet, this.buttonPressedBottomCells),
   this.lightningArtist = new SpriteSheetArtist(this.spritesheet, this.lightningCells),
   this.slowDownPowerUpArtist = new SpriteSheetArtist(this.spritesheet, this.slowDownPowerUpCells),
   this.doublePointsPowerUpArtist = new SpriteSheetArtist(this.spritesheet, this.doublePointsPowerUpCells),
   this.shieldPowerUpArtist = new SpriteSheetArtist(this.spritesheet, this.shieldPowerUpCells),
   this.playerArtist = new SpriteSheetArtist(this.spritesheet, this.playerCells),
   this.playerShieldArtist = new SpriteSheetArtist(this.spritesheet, this.playerShieldCells),

   this.collideBehavior = {
      execute: function (sprite, time, fps, context) {
         var otherSprite;

         for (var i=0; i < gravity.obstructions.length; ++i) { 
            otherSprite = gravity.obstructions[i];
            if (this.isCandidateForCollision(sprite, otherSprite)) {
               if (this.didCollide(sprite, otherSprite, context)) { 
                  this.processCollision(sprite, otherSprite);
               }
            }
         }

         for (var i=0; i < gravity.buttons.length; ++i) { 
            otherSprite = gravity.buttons[i];
            if (this.isCandidateForCollision(sprite, otherSprite)) {
               if (this.didCollide(sprite, otherSprite, context)) { 
                  this.processCollision(sprite, otherSprite);
               }
            }
         }

         for (var i=0; i < gravity.powerUps.length; ++i) { 
            otherSprite = gravity.powerUps[i];
            if (this.isCandidateForCollision(sprite, otherSprite)) {
               if (this.didCollide(sprite, otherSprite, context)) { 
                  this.processCollision(sprite, otherSprite);
               }
            }
         }

         for (var i=0; i < gravity.lightnings.length; ++i) { 
            otherSprite = gravity.lightnings[i];
            if (this.isCandidateForCollision(sprite, otherSprite)) {
               if (this.didCollide(sprite, otherSprite, context)) { 
                  this.processCollision(sprite, otherSprite);
               }
            }
         }
      },

      isCandidateForCollision: function (sprite, otherSprite) {
         return sprite !== otherSprite && !sprite.exploding;
      }, 

      didRunnerCollideWithOtherSprite: function (left, top, right, bottom,
                                                 centerX, centerY,
                                                 otherSprite, context) {

         context.beginPath();
         context.rect(otherSprite.left - gravity.spriteOffset, otherSprite.top,
                      otherSprite.width, otherSprite.height);

         return context.isPointInPath(left, top)        ||
                context.isPointInPath(right, top)       ||

                context.isPointInPath(centerX, centerY) ||

                context.isPointInPath(left, bottom)     ||
                context.isPointInPath(right, bottom);
      },
     
      didCollide: function (sprite, otherSprite, context) {
         var left = sprite.left+10,
             right = sprite.left + sprite.width+20,
             top = sprite.top,
             bottom = sprite.top + sprite.height + 5,
             centerX = left + sprite.width/2,
             centerY = sprite.top + sprite.height/2;

            return this.didRunnerCollideWithOtherSprite(left, top, right, bottom,
                                                  centerX, centerY,
                                                  otherSprite, context);
      },

      processCollision: function (sprite, otherSprite) {
         if (  'obstruction' === otherSprite.type) {

            gravity.explode(sprite);

         } else if( 'lightning' == otherSprite.type) {
            gravity.lightingHit(sprite);

         }else if ('button' === otherSprite.type) {
            gravity.buttonHit();

         } else if ('slow_down_power_up' === otherSprite.type) {
            gravity.removePowerUp();
            gravity.actvateSlowDown();

         } /*else if ('double_points_power_up' === otherSprite.type) {
            gravity.removePowerUp();
            gravity.activateDoublePoints();

         }*/ else if ('shield_power_up' === otherSprite.type && gravity.playerHasSheild == false) {
            gravity.removePowerUp();
            gravity.activateShield();
         }
      }
   };

   this.switchBehaviour = 
   {
      pause: function(sprite, now)
      {
         if(sprite.ascendTimer.isRunning())
         {
            sprite.ascendTimer.pause(now);
         }
         else if(sprite.descendTimer.isRunning())
         {
            sprite.descendTimer.pause(now);
         }
      },

      unpause: function(sprite, now)
      {
         if(sprite.ascendTimer.isRunning())
         {
            sprite.ascendTimer.unpause(now);
         }
         else if(sprite.descendTimer.isRunning())
         {
            sprite.descendTimer.unpause(now);
         }
      },

      isAscending: function(sprite)
      {
         return sprite.ascendTimer.isRunning();
      },

      ascend: function(sprite, now)
      {
         var elapsed = sprite.ascendTimer.getElapsedTime(now),
             deltaY = elapsed / (sprite.SWITCH_DURATION) * sprite.SWITCH_HEIGHT;
         sprite.top = sprite.verticalLaunchPosition - deltaY;
      },

      isDoneAscending: function(sprite, now)
      {
         return sprite.ascendTimer.getElapsedTime(now) > sprite.SWITCH_DURATION;
      },

      finishAscent: function(sprite, now)
      {
         sprite.ascendTimer.stop(now);
         gravity.playerTrack = gravity.TOP_TRACK_ID; 
         sprite.top = gravity.TOP_PLAYER_TRACK_Y;
         sprite.stopSwitching();
      },

      isDescending: function(sprite)
      {
         return sprite.descendTimer.isRunning();
      },

      descend: function(sprite, now)
      {
         var elapsed = sprite.descendTimer.getElapsedTime(now),
             deltaY = elapsed / (sprite.SWITCH_DURATION) * sprite.SWITCH_HEIGHT;
         sprite.top = sprite.verticalLaunchPosition + deltaY;
      },

      isDoneDescending: function(sprite, now)
      {
         return sprite.descendTimer.getElapsedTime(now) > sprite.SWITCH_DURATION;
      },

      finishDescent: function(sprite, now)
      {
         sprite.descendTimer.stop(now);
         gravity.playerTrack = gravity.BOTTOM_TRACK_ID;
         sprite.top = gravity.BOTTOM_PLAYER_TRACK_Y;
         sprite.stopSwitching();
      },

      execute: function(sprite, now, fps, context, lastAnimationFrameTime)
      {
         if(!sprite.switching)
         {
            return;
         }
         if(this.isAscending(sprite))
         {
            if(!this.isDoneAscending(sprite, now))
            {
               this.ascend(sprite, now);
            }
            else
            {
               this.finishAscent(sprite, now);
            }
         }
         else if(this.isDescending(sprite))
         {
            if(!this.isDoneDescending(sprite, now))
            {
               this.descend(sprite, now);
            }
            else
               this.finishDescent(sprite, now);
         }
      }
   };

   this.player = new Sprite('player', this.playerArtist, [ new Cycle(100, 10), this.collideBehavior, this.switchBehaviour ]);

   this.sprites = [ this.player ];  //Array for sprites

   this.explosionAnimator = new SpriteAnimator(
      this.explosionCells,        
      this.EXPLOSION_DURATION, 

      function (sprite, animator) { 
         sprite.exploding = false;
         sprite.artist.cellIndex = 0;
      }
   );
};

Gravity.prototype = {
// Drawing
   draw: function (now) {
      this.setVelocities();
      this.setOffsets();

      this.drawSpace();
      this.drawPlanet();
      this.drawBackground();
      this.drawPillars();   

      this.updatePowerUps();
      this.drawPowerUps();

      this.updateLightnings();
      this.drawLightnings();

      this.updateObstructions(now);
      this.drawObstructions();

      this.updateBottomObstructions(now);
      this.drawBottomObstructions();

      this.updateButtons();
      this.drawButtons();

      this.movePlayer(now, this.playerDirection);

      this.updatePlayer(now);
      this.drawPlayer();

      if(!gravity.paused)
      {
         this.drawPauseButton();
      }
   },

   setVelocities: function() 
   {
   
      this.DIFFICULTY_MULTIPLIER = 1.5 + (this.score/1350) - this.VELOCITY_DEDUCTION;
      this.SWITCH_DURATION = 450 - (this.score*1.5);

      this.setWallVelocity();
      this.setPillarVelocity();
      this.setPlanetVelocity();
      this.setSpaceVelocity();
      this.setObstructionVelocity();
   },

   setWallVelocity: function () {
      this.wallVelocity = this.bgVelocity * this.OBSTRUCTION_VELOCITY_MULTIPLIER * this.DIFFICULTY_MULTIPLIER;
   },

   setObstructionVelocity: function () {
      this.obstructionVelocity = this.bgVelocity * this.OBSTRUCTION_VELOCITY_MULTIPLIER * this.DIFFICULTY_MULTIPLIER;
   },

   setPillarVelocity: function () {
      this.pillarVelocity = this.bgVelocity * this.PILLAR_VELOCITY_MULTIPLIER * this.DIFFICULTY_MULTIPLIER;
   },

   setPlanetVelocity: function () {
      this.planetVelocity = this.bgVelocity * this.PLANET_VELOCITY_MULTIPLIER * this.DIFFICULTY_MULTIPLIER;
   },

   setSpaceVelocity: function () {
      this.spaceVelocity = this.bgVelocity * this.SPACE_VELOCITY_MULTIPLIER * this.DIFFICULTY_MULTIPLIER;
   },

   setOffsets: function () {
      this.setBackgroundOffset();
      this.setSpriteOffsets();
      this.setPillarOffsets();
      this.setPlanetOffsets();
      this.setSpaceOffsets();
      this.setObstructionOffsets();
      this.setBottomObstructionOffsets();
      this.setButtonOffsets();
      this.setLightningOffsets();
      this.setPowerUpOffsets();
   },

   setBackgroundOffset: function () {
      var offset = this.backgroundOffset + this.bgVelocity/this.fps;

      if (offset > 0 && offset < this.background1.width) 
      {
         this.backgroundOffset = offset;
      }
      else 
      {
         this.backgroundOffset = 0;
         this.onePassed += 1;
      }

      if(this.score >= 2500)
      {
         this.level = 3;
      }
      else if(this.score >= 1400)
      {
         this.level = 2;
      }
      else
      {
         this.level = 1;
      }

   },

   setPillarOffsets: function () {
      var offset = this.pillarOffset + this.pillarVelocity/this.fps;

      if (offset > 0 && offset < this.backgroundPillars.width) {
         this.pillarOffset = offset;
      }
      else {
         this.pillarOffset = 0;
      }
   },

   setPlanetOffsets: function () {
      var offset = this.planetOffset + this.planetVelocity/this.fps;
      this.planetOffset = offset;
   },

   setSpaceOffsets: function () {
      var offset = this.spaceOffset + this.spaceVelocity/this.fps;

      if (offset > 0 && offset < this.backgroundPillars.width) {
         this.spaceOffset = offset;
      }
      else {
         this.spaceOffset = 0;
      }
   },

   setSpriteOffsets: function () {
      var i, sprite;
   
      this.spriteOffset += this.wallVelocity / this.fps; 

      for (i=0; i < this.sprites.length; ++i) {
         sprite = this.sprites[i];
      
         if ('player' !== sprite.type) {
            sprite.offset = this.spriteOffset; 
         }
      }
   },

   setObstructionOffsets: function () {
      var i, obstruction;
   
      this.obstructionOffset += this.obstructionVelocity/ this.fps; 

      for (i=0; i < this.obstructions.length; ++i) {
         obstruction = this.obstructions[i];

         obstruction.offset = this.obstructionOffset; 
      }
   },

   setBottomObstructionOffsets: function() {
      var i, obstruction;
   
      this.bottomObstructionOffset += this.obstructionVelocity/ this.fps; 

      for (i=0; i < this.specailBottoms.length; ++i) {
         obstruction = this.specailBottoms[i];

         obstruction.offset = this.obstructionOffset; 
      }
   },

   setButtonOffsets: function () {
      var i, button;
   
      this.buttonOffset += this.obstructionVelocity/ this.fps; 

      for (i=0; i < this.buttons.length; ++i) {
         button = this.buttons[i];

         button.offset = this.buttonOffset; 
      }
   },

   setLightningOffsets: function () {
      var i, lightning;
   
      this.lightningOffset += this.obstructionVelocity/ this.fps; 

      for (i=0; i < this.lightnings.length; ++i) {
         lightning = this.lightnings[i];

         lightning.offset = this.lightningOffset; 
      }
   },

   setPowerUpOffsets: function () {
      var i, powerUp;
   
      this.powerUpOffset += this.obstructionVelocity/ this.fps; 

      for (i=0; i < this.powerUps.length; ++i) {
         powerUp = this.powerUps[i];

         powerUp.offset = this.powerUpOffset; 
      }
   },

   drawBackground: function () {
      this.context.save();
   
      this.context.globalAlpha = 1.0;
      this.context.translate(-this.backgroundOffset, 0);
      if(this.twoPassed >= 4)
      { 
         this.context.drawImage(this.background3, 0, 0, this.background1.width, this.background1.height);
         this.context.drawImage(this.background3, this.background1.width, 0, this.background1.width, this.background1.height);
      }
      else if(this.onePassed >= 3)
      {
         this.context.drawImage(this.background3, 0, 0, this.background1.width, this.background1.height);
         this.context.drawImage(this.background3, this.background1.width, 0, this.background1.width, this.background1.height);
      }
      else if(this.onePassed >= 2)
      {
         this.context.drawImage(this.background2, 0, 0, this.background1.width, this.background1.height);
         this.context.drawImage(this.background3, this.background1.width, 0, this.background1.width, this.background1.height);
      }
      else if(this.onePassed >= 1)
      {
         this.context.drawImage(this.background1, 0, 0, this.background1.width, this.background1.height);
         this.context.drawImage(this.background2, this.background1.width, 0, this.background1.width, this.background1.height);
      }
      else
      {
         this.context.drawImage(this.background1, 0, 0, this.background1.width, this.background1.height);
         this.context.drawImage(this.background1, this.background1.width, 0, this.background1.width, this.background1.height);
      }
      
      this.context.restore();
   },

   drawPillars: function () {
      this.context.save();
   
      this.context.globalAlpha = 1.0;
      this.context.translate(-this.pillarOffset, 0);

      this.context.drawImage(this.backgroundPillars, 0, 0, this.backgroundPillars.width, this.backgroundPillars.height);

      this.context.drawImage(this.backgroundPillars, this.backgroundPillars.width, 0, this.backgroundPillars.width+1, this.backgroundPillars.height);

      this.context.restore();
   },

   drawPlanet: function () {
      this.context.save();
   
      this.context.globalAlpha = 1.0;
      this.context.translate(-this.planetOffset, 0);

      this.context.drawImage(this.planet, 200, 0, this.backgroundPillars.width, this.backgroundPillars.height);

      this.context.restore();
   },

   drawSpace: function () {
      this.context.save();
   
      this.context.globalAlpha = 1.0;
      this.context.translate(-this.spaceOffset, 0);

      this.context.drawImage(this.space, 0, 0, this.backgroundPillars.width, this.backgroundPillars.height);

      this.context.drawImage(this.space, this.backgroundPillars.width, 0, this.backgroundPillars.width+1, this.backgroundPillars.height);

      this.context.restore();
   },

   drawGoCountOut: function() {
      this.context.save();
   
      this.context.drawImage(this.goCountOutImage, 0, 0, this.goCountOutImage.width, this.goCountOutImage.height);

      this.context.restore();
   },

   drawPauseButton: function() {
  
      this.context.save();
   
      this.context.drawImage(this.pauseButton, 725, 0, this.pauseButton.width, this.pauseButton.height);

      this.context.restore();
   },

   calculateFps: function (now) { 
      var fps = 1000/(now - this.lastAnimationFrameTime);
      this.lastAnimationFrameTime = now;

      if(now - this.lastFpsUpdateTime > 1000) { // Waits 1 second till it updates again
         this.lastFpsUpdateTime = now;
         this.fpsElement.innerHTML = fps.toFixed(0) + 'fps';
      }

      return fps;
   },

   calculateScore: function () { 
      this.score = this.score + this.POINTS_MULTIPLIER;
      console.log(this.POINTS_MULTIPLIER);
      this.scoreElement.innerHTML = (this.score/25).toFixed(0);

      return this.score;
   },

   calculateHighscore: function () { 
      if(this.score > this.highscore){
         this.highscore = this.score;
      }
      this.highscoreElement.innerHTML = 'Highscore: '+(this.highscore/25).toFixed(0);

      return this.highscore;
   },

// Toast
   displayToast: function (text, length) {
      length = length || this.DEFAULT_TOAST_TIME;

      toast.style.display = 'block';
      toast.innerHTML = text;

      setTimeout( function (e) {
         if (gravity.windowHasFocus) {
            toast.style.opacity = 1.0; // After toast is displayed
         }
      }, 500);

      setTimeout( function (e) {
         if (gravity.windowHasFocus) {
            toast.style.opacity = 0; // Starts CSS3 transition
         }

         setTimeout( function (e) { 
            if (gravity.windowHasFocus) {
               toast.style.display = 'none'; 
            }
         }, 480);
      }, length);
   },

//Reset
   restartGame: function(){
      this.resetOffsets();
      this.resetObstructions();
      this.resetBottomObstructions();
      this.resetButtons();
      this.resetLightnings();
      this.resetPowerUps();
      this.resetPlayer();
      if( this.score > this.highscore ) {
         this.highscore = this.score;
      }
      this.score = 0;
      this.POINTS_MULTIPLIER = 1;
   },

   resetOffsets: function () 
   {
      this.bgVelocity = 60;
      this.backgroundOffset = 0;
      this.pillarOffset = 0;
      this.spriteOffset = 0;
      this.obstructionOffset = 0;
      this.bottomObstructionOffset = 0;
      this.buttonOffset = 0;
      this.lightningOffset = 0;
      this.powerUpOffset = 0;
      this.onePassed = 0;
      this.VELOCITY_DEDUCTION = 0;
   },

   resetObstructions: function(){
      
      while (this.obstructions.length) {                             //this method is not as efficient as Eg: this.obstructions = [];
         this.obstructions.pop();                                    //Has to be used to keep the same array however
      }
      this.nextObstructionLeft = this.STARTING_DISTANCE_OBSTRUCTIONS;
      this.initialiseObstructionData();
   },

   resetBottomObstructions: function(){
      
      while (this.specailBottoms.length) {
         this.specailBottoms.pop();
      }
   },

   resetButtons: function(){
      while(this.buttons.length) {
         this.buttons.pop();
      }
   },

   resetPowerUps: function(){
      while(this.powerUps.length) {
         this.powerUps.pop();
      }
      this.nextPowerUpLeft = this.STARTING_DISTANCE_POWER_UPS;
      this.makePowerUpData();

   },

   resetLightnings: function(){
      while(this.lightnings.length) {
         this.lightnings.pop();
      }
   },

   resetPlayer: function() {
      this.player.velocityX = this.STARTING_PLAYER_VELOCITY;
      this.player.left = this.STARTING_PLAYER_LEFT;
      this.player.top = this.BOTTOM_PLAYER_TRACK_Y;
      this.playerTrack = this.BOTTOM_TRACK_ID,
      this.playerDirection = this.PLAYER_DIRECTION_MID,
      this.player.visible = true;
      this.player.exploding = false;
      this.player.artist.cells = this.playerCells;
      this.player.artist.cellIndex = 0;
      this.playerHasSheild = false;
   },

// Sprites
   startPlayer: function () {
      this.player.velocityX = this.STARTING_PLAYER_VELOCITY;
      this.player.left = this.STARTING_PLAYER_LEFT;
      this.player.top = this.BOTTOM_PLAYER_TRACK_Y;
      this.player.artist.cells = this.playerCells;
   },

   explode: function (sprite, silent) {
      this.stopVelocities();
      sprite.exploding = true;
      this.playSound(this.explodingSound);
      this.explosionAnimator.start(sprite, true);
      gravity.displayToast('YOU LOSE', 500); 
      setTimeout(function (e){
         gravity.restartGame();
      }, 500);
   },

   lightingHit: function(sprite) {
      console.log("lightning was hit!");
      this.playLightningSound();
      if(this.playerHasSheild)
      {
         gravity.removeLightning();
         gravity.deactivateShield();
      } else {
         gravity.explode(sprite);
      }
   },

   buttonHit: function() {

      this.playLightningSound();
      this.lightnings.pop();
      this.playSound(this.buttonSound); //play sound here button press

      var button;
      button = this.buttons[this.buttons.length-1];
      button.type = 'button_pressed';

      if(button.top == this.BOTTOM_BUTTON_TRACK_Y)
      {
         this.buttons[this.buttons.length-1].artist = this.buttonBottomPressedArtist;
      } else {
         this.buttons[this.buttons.length-1].artist = this.buttonTopPressedArtist;
      }
      
   },

   actvateSlowDown: function() {
      

      this.playSound(this.slowDownTimeSound);

      this.VELOCITY_DEDUCTION = this.VELOCITY_DEDUCTION + 0.1
      
   },

   activateDoublePoints: function() {
      this.playSound(this.twoTimesSound);
      this.POINTS_MULTIPLIER = 2;

      setTimeout( function(e) {
         this.POINTS_MULTIPLIER = 1;
      }, 2000);

   },

   activateShield: function() {
      this.playerHasSheild = true;
      this.player.artist = this.playerShieldArtist;
      //change player bottom track to fit shield
      this.playSound(this.twoTimesSound);
      //play sound here activate shield
   },

   deactivateShield: function() {
      this.playerHasSheild = false;
      this.player.artist = this.playerArtist;
      this.playSound(this.shieldPowerOffSound);
      //play sound here deactivate shield
   },

   movePlayer: function (now, playerDirection) { //Checks what direction to send player
      if (playerDirection === this.PLAYER_DIRECTION_ASCEND){
         this.ascendPlayer(now, this.player);
      }
      else if (playerDirection === this.PLAYER_DIRECTION_DESCEND){
         this.descendPlayer(now, this.player);
      }
      else return;
   },

   stopVelocities: function() {
      this.bgVelocity = 0;
      this.pillarVelocity = 0;
      this.planetVelocity = 0;
      this.spaceVelocity = 0;
      this.wallVelocity = 0;
      this.obstructionVelocity = 0;
      this.specailObstructionVelocity = 0;
   },


// Pause
   togglePaused: function () {
      var now = this.timeSystem.calculateGameTime();
      this.paused = !this.paused;

      if (this.paused) {
         this.pauseStartTime = now;
         gravity.displayPauseMenu(); //displays pauseMenu until it is removed
      }
      else {
         this.lastAnimationFrameTime += (now - this.pauseStartTime);
         gravity.removePauseMenu();

      }
   },

   displayPauseMenu: function()
   {
      //pauseMenu.fitScreen(cw,ch);
      gravity.pauseMenu.style.display = 'block'; // Activate pause menu element

      setTimeout( function (e) {
         if (gravity.windowHasFocus) {
            gravity.pauseMenu.style.opacity = 0.55; // After pause menu is displayed
         }
      }, 50);  
   },

   
   removePauseMenu: function()
   {
      gravity.drawGoCountOut();  
       var length = 100,
            PAUSE_MENU_FADE_OUT = 450;
      setTimeout( function (e) {
         if (gravity.windowHasFocus) {
            gravity.pauseMenu.style.opacity = 0; // Starts CSS3 transition
         }
         setTimeout( function (e) { 
            if (gravity.windowHasFocus) {
            gravity.pauseMenu.style.display = 'none'; 
          }
       }, PAUSE_MENU_FADE_OUT);
      }, length);
   },

   togglePauseCountOut: function()
   {
    gravity.drawGoCountOut();  
      setTimeout(function (e) {
         gravity.displayToast(' ', gravity.RESTART_TIME);

         setTimeout(function (e) {
            if ( gravity.windowHasFocus) {
               gravity.togglePaused();
            }
         }, gravity.RESTART_TIME);
      }, gravity.RESTART_TIME);
  // RESUME BUTTON ON PAUSE MENU
   },
   
// Moblie methods
   detectMoblie: function()
   {
      gravity.mobile = 'ontouchstart' in window;
      console.log(gravity.mobile);
   },

   fitScreen: function()
   {
      var arenaSize = gravity.calculateArenaSize(gravity.getViewportSize());
      gravity.resizeElementsToFitScreen(arenaSize.width, arenaSize.height);
   },

   calculateArenaSize: function(viewportSize)
   {
      var DESKTOP_ARENA_WIDTH = 800,
          DESKTOP_ARENA_HEIGHT = 400,
          arenaHeight,
          arenaWidth;
      //maintain aspect ratio
      arenaHeight = viewportSize.width*(DESKTOP_ARENA_HEIGHT/DESKTOP_ARENA_WIDTH);
      if(arenaHeight < viewportSize.height) //Height fits
      {
         arenaWidth = viewportSize.width;
      }
      else //Height does not fit
      {
         arenaHeight = viewportSize.height;
         arenaWidth = arenaHeight*(DESKTOP_ARENA_WIDTH/DESKTOP_ARENA_HEIGHT);
      }
      if(arenaWidth > DESKTOP_ARENA_WIDTH)
      {
         arenaWidth = DESKTOP_ARENA_WIDTH;
      }
      if(arenaHeight > DESKTOP_ARENA_HEIGHT)
      {
         arenaHeight = DESKTOP_ARENA_HEIGHT;
      }
      return { 
         width:  arenaWidth, 
         height: arenaHeight 
      };
   },

   resizeElementsToFitScreen: function(arenaWidth, arenaHeight)
   {
      gravity.resizeElement(document.getElementById('arena'), arenaWidth,
         arenaHeight);
      gravity.resizeElement(this.pauseMenu, arenaWidth,
         arenaHeight);
      //gravity.resizeElement(gravity.Toast, arenaWidth,
      //  arenaHeight);
   },

   getViewportSize: function()
   {
      return { 
         width: Math.max(document.documentElement.clientWidth 
         || window.innerWidth || 0), 
         height: Math.max(document.documentElement.clientHeight 
         || window.innerHeight || 0)};
   },

    resizeElement: function(element, w, h)
   {
      element.style.width = w + 'px';
      element.style.height = h + 'px';
   },

//Touch Controls
   addTouchEventHandlers: function()
   {
      gravity.canvas.addEventListener('touchstart', gravity.touchStart); // mouse down aswell
      gravity.canvas.addEventListener('touchend', gravity.touchEnd);
   },
   
   touchStart: function(e)
   {
      if(gravity.playing)
      {
         event.preventDefault(); // stop players from draggin the game canvas
      }
      
   },

   touchEnd: function(e)
   {
      var x = event.changedTouches[0].pageX;
      var y = event.changedTouches[0].pageY;
      console.log(x);
      console.log(y);
      console.log(gravity.canvas.width/2);
      if(!gravity.paused)
      {
         gravity.player.switch();
      }
      else
      {
          if(x < gravity.canvas.width/2 && y > gravity.canvas.height/2) // get the box for the buttons from switcher
         {
            gravity.restartGame();
            gravity.togglePauseCountOut();
            // setTimeout(function (e){
            // gravity.togglePaused();  // needed to restart
            console.log("Reset");
            // }, RESTART_TIME);// RESET BUTTON ON PAUSE MENU
            gravity.removePauseMenu();
         }
         else if(x > gravity.canvas.width/2 && y > gravity.canvas.height/2)
         {
            gravity.togglePauseCountOut();
            console.log("Resume");
         }
      }

      
      event.preventDefault();
      
   },

// CONTROLS
   MouseClickControls: function(e)
   {
      var x = event.clientX;
      var y = event.clientY;
      var CLICK_OFFSET = 250;
      
      if(!gravity.paused)
      {
         gravity.player.switch();
      }
      else if(gravity.paused)
      {
          if(x - CLICK_OFFSET < gravity.canvas.width/2 && y > gravity.canvas.height/2) // get the box for the buttons from switcher
         {

            gravity.restartGame();
            gravity.togglePauseCountOut(); // need to restart
            // setTimeout(function (e){
            // gravity.togglePaused();  
            console.log("Reset");
            // }, RESTART_TIME);// RESET BUTTON ON PAUSE MENU
            gravity.removePauseMenu();

         }
         else if(x - CLICK_OFFSET > gravity.canvas.width/2 && y > gravity.canvas.height/2)
         {

           gravity.togglePauseCountOut();
            console.log("Resume");
         }
      }
      event.preventDefault();
   },


// Animation
   animate: function (now) { // Game loop
      now = gravity.timeSystem.calculateGameTime();
      if (gravity.paused){
         setTimeout(function() {
            requestNextAnimationFrame(gravity.animate);
         }, gravity.PAUSE_CHECK_INTERVAL);
      }
      else {
         gravity.fps = gravity.calculateFps(now);
         gravity.score = gravity.calculateScore();
         gravity.highscore = gravity.calculateHighscore();
         gravity.draw(now);
         requestNextAnimationFrame(gravity.animate);
      }
   },

// Initialization
   initializeImages: function () { // loads images and runs startGame() once images are loaded
      this.planet.src = 'images/planet.png';
      this.space.src = 'images/space.png';
      this.background1.src = 'images/background_level_one_yellow.png';
      this.background2.src = 'images/background_level_one_orange.png';
      this.background3.src = 'images/background_level_one_red.png';
      this.goCountOutImage.src = 'images/Go.png';
      this.pauseButton.src = 'images/pauseButton.png';
      this.backgroundPillars.src = 'images/background_pillars.png';
      this.spritesheet.src = 'images/spritesheet.png';

      this.background1.onload = function (e) {
         gravity.startGame();
      };
   },

   start: function () {
      this.initializeImages();
      this.timeSystem.start();
      this.startMusic();
      this.setTimeRate(1.0);
      this.startPlayer();
      gravity.displayToast('Avoid the walls by pressing<br>SPACE', 3000);
   },

   startGame: function () {
      this.makePowerUpData();
      this.initialiseObstructionData();
      requestNextAnimationFrame(this.animate);
   },


   setTimeRate: function(rate)
   {
      this.timeRate = rate;
      this.timeSystem.setTransducer(function(now)
      {
         return now*gravity.timeRate;
      });
   },

//Button Data, update and draw methods

   makeButtonData: function(i)
   {
      var button = new Sprite('button', this.buttonTopArtist);

      button.width = gravity.BUTTON_CELLS_WIDTH;
      button.height = gravity.BUTTON_CELLS_HEIGHT;
      button.left = (gravity.nextObstructionLeft + gravity.SPECIAL_OBSTRUCTION_DISTANCE) - gravity.BUTTON_DISTANCE_FROM_OBSTRUCTION;

      if(Math.floor(Math.random() * 2) == 1)
      {

         button.top = this.TOP_BUTTON_TRACK_Y;
      } else {

         button.top = gravity.BOTTOM_BUTTON_TRACK_Y;
         button.artist = this.buttonBottomArtist;
      }
      
      this.buttons.unshift(button);
   },

   updateButtons: function(now) {
      var button;

      for (var i=0; i < this.buttons.length; ++i) {
         button = this.buttons[i];
         
         if (button.visible && this.spriteInView(button)) {

            button.update(now, this.fps, this.context);
         } else if(button.left - this.BUTTON_CELLS_WIDTH - button.offset < 0)
         {
            console.log("Button removed!");
            this.buttons.pop();
         }
      }
   },

   drawButtons: function()
   {
      var button;
   
      for (var i=0; i < this.buttons.length; ++i) {
         button = this.buttons[i];

         if (button.visible && this.spriteInView(button)) {
            this.context.translate(-button.offset, 0);

            button.draw(this.context);

            this.context.translate(button.offset, 0);
         }
      }
   },

   //Lightning Data, update and draw methods
   makeLightningData: function(i)
   {
      var lightning = new Sprite('lightning', this.lightningArtist, [new Cycle(100)]);

      lightning.artist.cells = this.lightningCells;

      lightning.width = gravity.LIGHTNING_CELLS_WIDTH;
      lightning.height = gravity.LIGHTNING_CELLS_HEIGHT;
      lightning.left = gravity.nextObstructionLeft + (gravity.SPECIAL_OBSTRUCTION_CELLS_WIDTH/2) - (gravity.LIGHTNING_CELLS_WIDTH/2);
      lightning.top = gravity.SPECIAL_OBSTRUCTION_CELLS_HEIGHT;
      
      this.lightnings.unshift(lightning);
   },

   removeLightning: function()
   {
      this.lightnings.pop();
   },

   updateLightnings: function(now) {
      var lightning;

      for (var i=0; i < this.lightnings.length; ++i) {
         lightning = this.lightnings[i];
         
         if (lightning.visible && this.spriteInView(lightning)) {

            lightning.update(now, this.fps, this.context);
         } 
         else if(lightning.left - this.LIGHTNING_CELLS_WIDTH - lightning.offset < 0)
         {
            gravity.removeLightning();
         }
      }
   },

   drawLightnings: function()
   {
      var lightning;
   
      for (var i=0; i < this.lightnings.length; ++i) {
         lightning = this.lightnings[i];

         if (lightning.visible && this.spriteInView(lightning)) {
            this.context.translate(-lightning.offset, 0);

            lightning.draw(this.context);
            
            this.context.translate(lightning.offset, 0);
         }
      }
   },

   //Obstructions Data, update and draw
   initialiseObstructionData: function()
   {
      var sprite;

      while(this.obstructions.length < 10 || this.obstructions.length == null)
      {
         sprite = new Sprite('obstruction', this.obstructionYellowArtist);
                      
         //new sprite to be definied inside while loop.
         if(Math.floor(Math.random() * 2) == 1)                                        //Javascript arrays store references to objects,
         {                                                                             //all values would come out the same if not in the loop.

            sprite.top = gravity.TOP_OBSTRUCTION_TRACK_Y;
         } else {

            sprite.top = gravity.BOTTOM_OBSTRUCTION_TRACK_Y;
         }

         sprite.top = this.checkObstructionSide(sprite.top);

         sprite.width = gravity.OBSTRUCTION_CELLS_WIDTH;
         sprite.height = gravity.OBSTRUCTION_CELLS_HEIGHT;

         gravity.nextObstructionLeft += gravity.OBSTRUCTION_DISTANCE;
         sprite.left = gravity.nextObstructionLeft;

         this.obstructions.push(sprite);
      }
   },

   updateObstructionData: function(i)
   {
      if(this.level === 3)
      {
         this.obstructions[i] = new Sprite('obstruction', this.obstructionRedArtist);
      }
      else if(this.level === 2)
      {
         this.obstructions[i] = new Sprite('obstruction', this.obstructionOrangeArtist);
      }
      else
      {
         this.obstructions[i] = new Sprite('obstruction', this.obstructionYellowArtist);
      }
      console.log(this.level);
      if(Math.floor(Math.random() * 2) == 1)
      {
         this.obstructions[i].top = gravity.TOP_OBSTRUCTION_TRACK_Y;
      } else {
         this.obstructions[i].top = gravity.BOTTOM_OBSTRUCTION_TRACK_Y;
      }

      this.obstructions[i].top = this.checkObstructionSide(this.obstructions[i].top);

      this.obstructions[i].width = gravity.OBSTRUCTION_CELLS_WIDTH;
      this.obstructions[i].height = gravity.OBSTRUCTION_CELLS_HEIGHT;

      gravity.nextObstructionLeft += gravity.OBSTRUCTION_DISTANCE;
      this.obstructions[i].left = gravity.nextObstructionLeft;
   },

   makeSpecialObstructionData: function(i)
   {
      this.obstructions[i] = new Sprite('special_obstruction', this.specialObstructionTopArtist);
      this.obstructions[i].top = gravity.SPECIAL_OBSTRUCTION_TOP_Y;
      this.obstructions[i].width = gravity.SPECIAL_OBSTRUCTION_CELLS_WIDTH;
      this.obstructions[i].height = gravity.SPECIAL_OBSTRUCTION_CELLS_HEIGHT;

      this.makeButtonData(i);

      gravity.nextObstructionLeft += gravity.SPECIAL_OBSTRUCTION_DISTANCE;
      this.obstructions[i].left = gravity.nextObstructionLeft;
      this.makeLightningData();
      this.makeSpecialObstructionBottomData();
   },

   updateObstructions: function (now) {
      var obstruction;

      for (var i=0; i < this.obstructions.length; ++i) {
         obstruction = this.obstructions[i];
         
         if (obstruction.visible && this.spriteInView(obstruction)) {

            obstruction.update(now, this.fps, this.context);
         } else if(obstruction.left - this.OBSTRUCTION_CELLS_WIDTH - obstruction.offset < 0)
         {

            if(Math.floor(Math.random() * this.SPECIAL_OBSTRUCTION_FREQUENCY_CHANCE) + 1 == 1)
            {
               this.makeSpecialObstructionData(i);
            } else {
               this.updateObstructionData(i);
            }
         }
      }
   },

   drawObstructions: function() {
      var obstruction;
   
      for (var i=0; i < this.obstructions.length; ++i) {
         obstruction = this.obstructions[i];

         if (obstruction.visible && this.spriteInView(obstruction)) {
            this.context.translate(-obstruction.offset, 0);

            obstruction.draw(this.context);

            this.context.translate(obstruction.offset, 0);
         }
      }
   },

   checkObstructionSide: function(track) {

      if(track == this.BOTTOM_OBSTRUCTION_TRACK_Y)
      {
         if(this.side == this.BOTTOM_OBSTRUCTION && this.sameSideObstacle > 1)
         {
            this.sameSideObstacle = 1;
            this.side = this.TOP_OBSTRUCTION;
            return this.TOP_OBSTRUCTION_TRACK_Y;

         } else if (this.side == this.TOP_OBSTRUCTION){
            this.sameSideObstacle = 1;
            this.side = this.BOTTOM_OBSTRUCTION;
         } else {
            this.sameSideObstacle++;
         }

      } else {

         if(this.side == this.TOP_OBSTRUCTION && this.sameSideObstacle > 1)
         {
            this.sameSideObstacle = 1;
            this.side = this.BOTTOM_OBSTRUCTION;
            return this.BOTTOM_OBSTRUCTION_TRACK_Y;

         } else if (this.side == this.BOTTOM_OBSTRUCTION){
            this.sameSideObstacle = 1;
            this.side = this.TOP_OBSTRUCTION;
         } else {
            this.sameSideObstacle++;
         }
      }

      return track;
   },

   //Special Bottom obstruction data
   makeSpecialObstructionBottomData: function()
   {
      sprite = new Sprite('special_obstruction', this.specialObstructionBottomArtist);
      sprite.top = gravity.LIGHTNING_TRACK_Y + gravity.LIGHTNING_CELLS_HEIGHT;
      sprite.width = gravity.SPECIAL_OBSTRUCTION_CELLS_WIDTH;
      sprite.height = gravity.SPECIAL_OBSTRUCTION_CELLS_HEIGHT;

      sprite.left = gravity.nextObstructionLeft;
      this.specailBottoms.unshift(sprite);
   },


   updateBottomObstructions: function(now) {
      var obstruction;

      for (var i=0; i < this.specailBottoms.length; ++i) {
         obstruction = this.specailBottoms[i];
         
         if (obstruction.visible && this.spriteInView(obstruction)) {

            obstruction.update(now, this.fps, this.context);
         } else if(obstruction.left - this.SPECIAL_OBSTRUCTION_CELLS_WIDTH - obstruction.offset < 0)
         {
            this.specailBottoms.pop();
         }
      }
   },

   drawBottomObstructions: function() {
      var obstruction;
   
      for (var i=0; i < this.specailBottoms.length; ++i) {
         obstruction = this.specailBottoms[i];

         if (obstruction.visible && this.spriteInView(obstruction)) {
            this.context.translate(-obstruction.offset, 0);

            obstruction.draw(this.context);

            this.context.translate(obstruction.offset, 0);
         }
      }
   },

//Power Up functions
   makePowerUpData: function()
   {
      var powerUp, chance;

      chance = Math.floor(Math.random() * 3) + 1;

      if(chance == 1)
      {
         powerUp = new Sprite('slow_down_power_up', this.slowDownPowerUpArtist);
      } else {
         powerUp = new Sprite('shield_power_up', this.shieldPowerUpArtist);
      }
      /*} else {
         powerUp = new Sprite('double_points_power_up', this.doublePointsPowerUpArtist);
      }*/

      gravity.nextPowerUpLeft += gravity.POWER_UP_FREQUENCY;
      powerUp.left = gravity.nextPowerUpLeft;
      powerUp.top = gravity.POWER_UP_TRACK_Y;
      powerUp.width = gravity.POWER_UP_CELLS_WIDTH;
      powerUp.height = gravity.POWER_UP_CELLS_HEIGHT;

      this.powerUps.unshift(powerUp);
   },

   removePowerUp: function() {
      this.powerUps.pop();
      gravity.makePowerUpData();
   },

   updatePowerUps: function(now) {
      var powerUp;

      for (var i=0; i < this.powerUps.length; ++i) {
         powerUp = this.powerUps[i];

         if (powerUp.visible && this.spriteInView(powerUp)) {

            powerUp.update(now, this.fps, this.context);
         } else if(powerUp.left + this.POWER_UP_CELLS_WIDTH - powerUp.offset < 0)
         {
            gravity.removePowerUp();
         }
      }
   },

   drawPowerUps: function()
   {
      var powerUp;
   
      for (var i=0; i < this.powerUps.length; ++i) {
         powerUp = this.powerUps[i];

         if (powerUp.visible && this.spriteInView(powerUp)) {
            this.context.translate(-powerUp.offset, 0);

            powerUp.draw(this.context);

            this.context.translate(powerUp.offset, 0);
         }
      }
   },

//Player update and draw methods
   updatePlayer: function (now) {
      var sprite = this.player;

      if (sprite.visible && this.spriteInView(sprite)) {
            sprite.update(now, this.fps, this.context);
      }
   },
   
   drawPlayer: function() {
      var sprite = this.player;
   
      if (sprite.visible && this.spriteInView(sprite)) {
         this.context.translate(-sprite.offset, 0);

         sprite.draw(this.context);

         this.context.translate(sprite.offset, 0);
      }
   },
   
   spriteInView: function(sprite) {
      return sprite === this.player || 
         (sprite.left + sprite.width > this.spriteOffset &&
          sprite.left < this.spriteOffset + this.canvas.width);   
   },

   createSprites: function() {

      this.equipPlayerForSwitchingTrack();
   },

   equipPlayerForSwitchingTrack: function()
   {
      var INITIAL_TRACK = this.playerTrack,
         TOP_TRACK_ID = this.TOP_TRACK_ID,
         BOTTOM_TRACK_ID = this.BOTTOM_TRACK_ID,

         
      PLAYER_SWITCH_HEIGHT = this.BOTTOM_PLAYER_TRACK_Y - this.TOP_PLAYER_TRACK_Y,
      PLAYER_SWITCH_DURATION = 450;

      this.player.SWITCH_DURATION = PLAYER_SWITCH_DURATION;
      this.player.SWITCH_HEIGHT = PLAYER_SWITCH_HEIGHT;

      this.player.switching = false;

      this.player.ascendTimer = new AnimationTimer(this.player.SWITCH_DURATION,
         AnimationTimer.makeEaseInEasingFunction(1.1));
      this.player.descendTimer = new AnimationTimer(this.player.SWITCH_DURATION,
         AnimationTimer.makeEaseInEasingFunction(1.1));


       this.player.switch = function()
      {
         if(this.switching)
         {
            return;
         }
         this.switching = true;
         this.runAnimationRate = 0;
         INITIAL_TRACK = gravity.playerTrack;

         if(INITIAL_TRACK === BOTTOM_TRACK_ID)
         {

            this.ascendTimer.start(gravity.timeSystem.calculateGameTime());
            this.verticalLaunchPosition = this.top;
         }
         else if(INITIAL_TRACK === TOP_TRACK_ID)
         {
            this.descendTimer.start(gravity.timeSystem.calculateGameTime());
            this.verticalLaunchPosition = this.top;
         }
      },

      this.player.stopSwitching = function()
      {
         this.ascendTimer.stop();
         this.descendTimer.stop();
         this.switching = false;
      };
   },

//startMusic
   startMusic: function()
   {
      var MUSIC_DELAY = 1000;

      setTimeout(function()
      {
         gravity.musicElement.play();

         gravity.pollMusic();
      }, MUSIC_DELAY);
   },

   pollMusic: function()
   {
      var POLL_INTERVAL = 500,
      SOUNDTRACK_LENGTH = 132,
      timerID;

      timerID = setInterval(function()
      {
         if(gravity.musicElement.currentTime > SOUNDTRACK_LENGTH)
         {
            clearInterval(timerID); //stop polling
            gravity.restartMusic();
         }
      }, POLL_INTERVAL);
   },
   restartMusic: function()
   {
      gravity.musicElement.pause();
      gravity.musicElement.currentTime = 0;
      gravity.startMusic();
   },

   createAudioChannels: function()
   {
      var channel;

      for(var i=0; i < this.audioChannels.length; ++i)
      {
         channel = this.audioChannels[i];
         if(i !== 0)
         {
            channel.audio = document.createElement('audio');
            channel.audio.addEventListener('loaddata',
               this.soundLoaded, false);
            channel.audio.src = this.audioSprites.currentSrc;
         }
         channel.audio.autobuffer = true;
      }
   },

   soundLoaded: function()
   {
      gravity.audioSpriteCountdown--;
      if(gravity.audioSpriteCountdown === 0)
      {
         if(!gravity.gameStarted && gravity.graphicsReady)
         {
            gravity.startGame();
         }
      }
   },

   playSound: function(sound)
   {
      var channel,
          audio;

      if(this.soundOn)
      {
         channel = this.getFirstAvailableAudioChannel();

         if(!channel)
         {
            if(console)
            {
               console.warn('All audio channels are busy. Cant play sound - ' + sound);
            }
         }
         else
         {
            audio = channel.audio;
            audio.volume = sound.volume;

            this.seekAudio(sound, audio);
            this.playAudio(audio, channel);

             setTimeout(function () {
               channel.playing = false;
               gravity.seekAudio(sound, audio);
            }, sound.duration);
         }
      }
   },

   getFirstAvailableAudioChannel: function()
   {
      for(var i =0; i < this.audioChannels.length; ++i)
      {
         if(!this.audioChannels[i].playing)
         {
            console.log(i);
            return this.audioChannels[i];
         }
      }
      return null;
   },

   seekAudio: function(sound, audio)
   {
      try
      {
         audio.pause();
         audio.currentTime = sound.position;
      }
      catch(e)
      {
         console.error('Cannot play audio');
      }
   },

   playAudio: function(audio, channel)
   {
      try
      {
         audio.play();
         channel.playing = true;
      }
      catch(e)
      {
         if(console)
         {
            console.error('Cannot play audio');
         }
      }
   },

   playLightningSound: function()
   {
      if(this.lightningPlaying == false)
      {
         
         this.lightningPlaying = true;
         this.playSound(this.lightSound);

         setTimeout(function(e) {
            this.lightningPlaying = false;
         }, 600);
      }
   },

   detectMobile: function()
   {
      gravity.mobile = 'ontouchstart' in window;
      console.log(gravity.mobile);
   }
};

// Launch game
var gravity = new Gravity();
gravity.start();
gravity.createSprites();
//MUSIC AND SOUND
gravity.createAudioChannels();
gravity.musicElement.play();
gravity.soundOn = true;

gravity.fitScreen();
window.addEventListener("resize", gravity.fitScreen());
window.addEventListener("orientationchange", gravity.fitScreen());

var   SPACEBAR = 32,
      P = 80,
      R = 82;

//Keys
window.onkeydown = function (e) { // when a key is pressed it checks if it is 'p'
   var key = e.keyCode;           // if so pauses the game

   if (key === P || (gravity.paused && key != P)) { 
      gravity.togglePaused();
   }
	if (key === R) { 

      gravity.restartGame(); 
   }
   if (key === SPACEBAR) {  // 'SPACE'
      gravity.player.switch();
   }
};


window.onblur = function (e) {  // pauses when looking at different window
   gravity.windowHasFocus = false;
   
   if (!gravity.paused) {
      gravity.togglePaused();
   }
};

window.onfocus = function (e) {  // unpauses when go back to the window
   gravity.windowHasFocus = true;

   if (gravity.paused) {
      gravity.togglePauseCountOut();

   }
};

gravity.detectMoblie();

//Moblie
if(gravity.mobile)
{
   //gravity.instructionsElement = document.getElementById('gravity-mobile-instructions');
   gravity.addTouchEventHandlers();
}
else
{
   window.onclick = function (e)
   {
      gravity.MouseClickControls();
   }
}

